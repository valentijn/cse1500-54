/*
 * This file defines the class to deal with chess piece. It will only ever
 * export the object Piece
 *
 */
import { colours } from './board_configuration.mjs';

// Don't allow someone to change the variable
// It would be nicer to use integers instead
export const pieceTypes = {
    pawn: "pawn",
    rook: "rook",
    knight: "knight",
    bishop: "bishop",
    queen: "queen",
    king: "king",
};

const directions = {horizontal: 0, vertical: 0, diagonal: 0};

// Turn the object entirely immutable
Object.freeze(pieceTypes);

var piece = {
    initialize: function (colour, location) {
        this.colour = colour;

        // Since we are working with an array we want to offset it by one
        this.verticalPosition = parseInt(location.slice(1)) - 1;
        this.horizontalPosition = location.charAt(0).charCodeAt(0) - 'A'.charCodeAt(0);
        return this;
    },
    toString: function (key) {
        let char = String.fromCharCode('A'.charAt().charCodeAt(0) + this.horizontalPosition);
        let num = this.verticalPosition+1;
        return char + num;
    },

    toJSON: function (key) {
        return {
            colour: this.colour,
            verticalPosition: this.verticalPosition,
            horizontalPosition: this.horizontalPosition,
            type: this.type,
        };
    },

    // Every piece will contain a copy of the game global variable
    // so we can see the global state.
    game: undefined,

    checkIfMoveable: function(piece) {
        if (piece === undefined)
            return true;
        if (piece === null)
            return false;
        if (piece.colour !== this.colour)
            return true;
        return false;
    },

    getLocation: function(vOffset=0, hOffset=0) {
        let vPosition = this.verticalPosition+vOffset;
        let hPosition = this.horizontalPosition+hOffset;

        if (vPosition >= this.board.length || hPosition >= this.board.width ||
            hPosition < 0 || vPosition < 0)
            return null;

        return this.board[vPosition][hPosition];
    },

    // A generic move function which will work in some, but not all,
    // situations. In that case it will be overwritten by some
    // child object.
    getMoves: function () {
        let moves = [];

        /*
          This could be somewhat easily refactored but it takes some time
          and thinking I would rather not do right now.
        */

        // Check in the vertical direction
        for (let i = this.verticalPosition+1, c = 0;
             i < this.board.length && c < this.directions.vertical;
             i++, c++) {

            let location = this.board[i][this.horizontalPosition];
            if (location !== undefined) {
                if (this.colour !== location.colour)
                    moves.push([i, this.horizontalPosition]);
                break;
            }

            moves.push([i, this.horizontalPosition]);
        }

        for (let i = this.verticalPosition-1, j = this.directions.vertical, c = 0;
             i >= 0 && j > 0 && c < this.directions.vertical;
             i--, j--, c++) {
            let location = this.board[i][this.horizontalPosition];
            if (location !== undefined) {
                if (this.colour !== location.colour)
                    moves.push([i, this.horizontalPosition]);
                break;
            }

            moves.push([i, this.horizontalPosition]);
        }

        // Check in the horizontal direction
        for (let i = this.horizontalPosition+1, c = 0;
             i < this.board.width && c < this.directions.horizontal;
             i++, c++) {
            let location = this.board[this.verticalPosition][i];
            if (location !== undefined) {
                if (this.colour !== location.colour)
                    moves.push([this.verticalPosition, i]);
                break;
            }

            moves.push([this.verticalPosition, i]);
        }

        for (let i = this.horizontalPosition-1, j = this.directions.horizontal, c = 0;
             i >= 0 && j > 0 && c < this.directions.horizontal;
             i--, j--, c++) {
            let location = this.board[this.verticalPosition][i];
            if (location !== undefined) {
                if (this.colour !== location.colour)
                    moves.push([this.verticalPosition, i]);
                break;
            }

            moves.push([this.verticalPosition, i]);
        }

        // Diagonal movement
        for (let i = this.verticalPosition+1, j = this.horizontalPosition+1, c = 0;
             i < this.board.length && j < this.board.width && c < this.directions.diagonal;
             i++, j++, c++) {
            let location = this.board[i][j];
            if (location !== undefined) {
                if (this.colour !== location.colour)
                    moves.push([i, j]);
                break;
            }
            moves.push([i, j]);
        }

        for (let i = this.verticalPosition-1, j = this.horizontalPosition-1, c = 0;
             i >= 0 && j >= 0 && c < this.directions.diagonal;
             i--, j--, c++) {
            let location = this.board[i][j];
            if (location !== undefined) {
                if (this.colour !== location.colour)
                    moves.push([i, j]);
                break;
            }
            moves.push([i, j]);
        }

        for (let i = this.verticalPosition-1, j = this.horizontalPosition+1, c = 0;
             i >= 0 && j < this.board.width && c < this.directions.diagonal ;
             i--, j++, c++) {
            let location = this.board[i][j];
            if (location !== undefined) {
                if (this.colour !== location.colour)
                    moves.push([i, j]);
                break;
            }
            moves.push([i, j]);
        }

        for (let i = this.verticalPosition+1, j = this.horizontalPosition-1, c = 0;
             i < this.board.length && j >= 0 && c < this.directions.diagonal;
             i++, j--, c++) {
            let location = this.board[i][j];
            if (location !== undefined) {
                if (this.colour !== location.colour)
                    moves.push([i, j]);
                break;
            }
            moves.push([i, j]);
        }

        return moves;
    },

    // Move the piece to the designed place and return itself.
    // This allows us to chain calls.
    move: function(verticalPosition, horizontalPosition) {
        this.board[this.verticalPosition][this.horizontalPosition] = undefined;

        this.verticalPosition = verticalPosition;
        this.horizontalPosition = horizontalPosition;

        if (this.board[verticalPosition][horizontalPosition] !== undefined &&
            this.board[verticalPosition][horizontalPosition].type === "king")
            return "check";

        this.board[verticalPosition][horizontalPosition] = this;
        return this;
    },

};

var knight = Object.create(piece);
knight.type = pieceTypes.knight;

knight.getMoves = function() {
    let moves = [];
    let checkIfMoveable = (vOffset=0, hOffset=0) => {
        return this.checkIfMoveable(this.getLocation(vOffset, hOffset));
    };

    if (checkIfMoveable(1, 2))
        moves.push([this.verticalPosition+1, this.horizontalPosition+2]);

    if (checkIfMoveable(1, -2))
        moves.push([this.verticalPosition+1, this.horizontalPosition-2]);

    if (checkIfMoveable(-1, 2))
        moves.push([this.verticalPosition-1, this.horizontalPosition+2]);

    if (checkIfMoveable(-1, -2))
        moves.push([this.verticalPosition-1, this.horizontalPosition-2]);

    if (checkIfMoveable(2, 1))
        moves.push([this.verticalPosition+2, this.horizontalPosition+1]);

    if (checkIfMoveable(2 -1))
        moves.push([this.verticalPosition+2, this.horizontalPosition-1]);

    if (checkIfMoveable(-2, 1))
        moves.push([this.verticalPosition-2, this.horizontalPosition+1]);

    if (checkIfMoveable(-2, -1))
        moves.push([this.verticalPosition-2, this.horizontalPosition-1]);

    console.log(moves);

    return moves;
};

var pawn = Object.create(piece);
pawn.type = pieceTypes.pawn;

pawn.getMoves = function() {
    let moves = [];
    let checkIfHitable = (piece) => {
        if (piece === undefined || piece === null)
            return false;
        return piece.colour !== this.colour;
    };

    // We need to make sure that the pawn can only ever move in one direction
    let colorFix = (num) => { return (this.colour === colours.white) ? - num : num; };
    const one = colorFix(1);
    const two = colorFix(2);

    // Deal witht the special case where we can kill another piece
    if (checkIfHitable(this.getLocation(one, -1)))
        moves.push([this.verticalPosition+one, this.horizontalPosition-1]);

    if (checkIfHitable(this.getLocation(one, 1)))
        moves.push([this.verticalPosition+one, this.horizontalPosition+1]);

    if (this.getLocation(one) === undefined) {
        moves.push([this.verticalPosition+one, this.horizontalPosition]);
    } else {
        return moves;
    }

    if (this.getLocation(two) === undefined) {
        moves.push([this.verticalPosition+two, this.horizontalPosition]);
    }

    return moves;
};


var bishop = Object.create(piece);
bishop.directions = Object.create(directions);
bishop.directions.diagonal = Infinity;
bishop.type = pieceTypes.bishop;

var rook = Object.create(piece);
rook.type = pieceTypes.rook;
rook.directions = Object.create(directions);
rook.directions.vertical = Infinity;
rook.directions.horizontal = Infinity;

var queen = Object.create(piece);
queen.directions = Object.create(directions);
queen.directions.vertical = queen.directions.horizontal = queen.directions.diagonal = Infinity;
queen.type = pieceTypes.queen;

var king = Object.create(piece);
king.directions = Object.create(directions);
king.directions.vertical = king.directions.horizontal = king.directions.diagonal = 1;
king.type = pieceTypes.king;

export var pieces = {pawn, rook, knight, bishop, queen, king};
