var webSocket = new WebSocket(`ws://${window.location.hostname}:8080`);
var gameId = undefined;
var userId = undefined;
var board = undefined;
var lastSelectedPiece = undefined;
var awesomeSong = new Audio("/audios/diesel.ogg");

var settings = {
    flashSpeed: localStorage.getItem("flash-speed"),
    lecturerMode:localStorage.getItem("lecture-mode"),
    boringMode: localStorage.getItem("boring-mode"),
};

var possibleMoves = {
    movesList: [],

    add: function(piece) {
        piece.classList.add("possibleMove");
        this.movesList.push(piece);
    },

    clear: function () {
        this.movesList.forEach((btn) => {
            btn.disabled = false;
            btn.classList.remove("possibleMove");
        });
        this.movesList = [];
    }
};

awesomeSong.addEventListener("canplaythrough", event => {
    // awesomeSong.play();
});

function titleClicked() {
    $("#main-content")[0].requestFullscreen();
};

function sendWebMessage(type, content) {
    let request = {};
    request.messageType = type;
    request.userId = userId;
    request.gameId = gameId;
    request.content = content;

    // if (webSocket.readState)
    webSocket.send(JSON.stringify(request));
}

function buttonPressed(btn) {
    possibleMoves.clear();
    var content = {
        type: btn.classList[0],
        colour: parseInt(btn.dataset['colour']),
        verticalPosition: parseInt(btn.dataset['row']),
        horizontalPosition: parseInt(btn.dataset['column'])
    };

    lastSelectedPiece = btn;
    sendWebMessage("requestMoves", content);
};

function movePiece(btn) {
    var content = {
        type: lastSelectedPiece.class,
        colour: parseInt(lastSelectedPiece.dataset['colour']),
        oldLocation: {
            verticalPosition: parseInt(lastSelectedPiece.dataset['row']),
            horizontalPosition: parseInt(lastSelectedPiece.dataset['column'])
        },
        newLocation: {
            verticalPosition: parseInt(btn.dataset['row']),
            horizontalPosition: parseInt(btn.dataset['column']),
        }
    };

    sendWebMessage("move", content);
};

function toChessFormat(verticalPosition, horizontalPosition) {
    let char = String.fromCharCode('A'.charAt().charCodeAt(0) + horizontalPosition);
    let num = verticalPosition+1;
    return char + num;
}

function toIntegerLocation(chessFormat) {
    let horizontalPosition = chessFormat.charCodeAt(0) - 'A'.charAt().charCodeAt(0);
    let verticalPosition = parseInt(charFormat.charAt(1));
    return [horizontalPosition, verticalPosition];
}

function initialize(game) {
    let content = game['content'];
    gameId = content['gameId'];
    userId = content['userId'];
    board = content['pieces'];

    board.forEach(row => {
        row.forEach(column => {
            if (column) {
                let piece = $("#" + toChessFormat(column.verticalPosition, column.horizontalPosition))[0];
                piece.classList.add(column.type);
                piece.disabled = false;
                piece.dataset['colour'] = column.colour;
                piece.classList.add(column.colour === 0 ? "white" : "black");
            }
        });
    });
};

function getLocation(locationArr) {
    let chessFmt = toChessFormat(locationArr[0], locationArr[1]);
    return $(`#${chessFmt}`)[0];
}

var messageHandler = new Map();
messageHandler.set("requestMoves", function(content) {
    content.locations.forEach((location) => {
        let piece = getLocation(location);
        piece.disabled = false;
        piece.onclick = () => movePiece(piece) ;
        possibleMoves.add(piece);
    });
});

messageHandler.set("victory", function(content) {

    alert("You won!");
    window.location = "/";
});

messageHandler.set("update", function(log) {
    console.log(log);
    possibleMoves.clear();
    let oldLocation = getLocation([log.oldLocation.verticalPosition, log.oldLocation.horizontalPosition]);
    let newLocation = getLocation([log.newLocation.verticalPosition, log.newLocation.horizontalPosition]);
    newLocation.className = oldLocation.className;
    oldLocation.className = "";
    newLocation.dataset['colour'] = oldLocation.dataset['colour'];
    oldLocation.dataset['colour'] = "";
    oldLocation.disabled = true;
    newLocation.onclick = () => buttonPressed(newLocation);
    newLocation.disabled = false;
});

messageHandler.set("rejectMove", function(foo) {
    console.debug("Movement rejected");
    possibleMoves.clear();
});

webSocket.onmessage = (message) => {
    let parsed = JSON.parse(message.data);
    console.log(parsed);
    messageHandler.get(parsed.messageType)(parsed.content);
};

webSocket.onopen = () => {
    webSocket.send(JSON.stringify({
        messageType: 'joinGame',
        userId: undefined,
        gameId: window.location.pathname.slice(-1),
        content: {

        }
    }));
};

function showSettings() {
    $("#settings-screen").css("display", "flex");
    $("#overlay").css("display", "initial").click(hideSettings);
}

function hideSettings() {
    $("#settings-screen").css("display", "none");
    $("#overlay").css("display", "none");
}

webSocket.onclose = () => {
    alert("Connection with server lost!");
};

function smallerAndLarger(element) {
    element.animate([{transform: "rotateX(0deg)"}, {transform: "rotateX(360deg)"}], 1000);
}

$.getJSON(`/game/${window.location.pathname.slice(-1)}/getGame`, initialize);

$("#flash-speed")[0].value = settings.flashSpeed;
$("#lecturer-mode")[0].checked = settings.lecturerMode;
$("#boring-mode")[0].checked = settings.boringMode;

document.documentElement.style.setProperty("--speed", settings.flashSpeed + "s");
window.onunload = () => {
    sendWebMessage("exit", {});

    localStorage.setItem("flash-speed", settings.flashSpeed);
    localStorage.setItem("lecture-mode", settings.lectureMode);
    localStorage.setItem("boring-mode", settings.boringMode);
};

function changeFlashSpeed(range) {
    settings.flashSpeed = range.value;
    document.documentElement.style.setProperty("--speed", range.value + "s");
}
