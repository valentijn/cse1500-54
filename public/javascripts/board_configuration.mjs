const colours = {
    white: 0,
    black: 1,
};

Object.freeze(colours);


const boardConfiguration = {
    'standard': {
        length: 8,
        width: 8,
        setup: [
            {type: "pawn", colour: colours.black, location: "A2"},
            {type: "pawn", colour: colours.black, location: "B2"},
            {type: "pawn", colour: colours.black, location: "C2"},
            {type: "pawn", colour: colours.black, location: "D2"},
            {type: "pawn", colour: colours.black, location: "E2"},
            {type: "pawn", colour: colours.black, location: "F2"},
            {type: "pawn", colour: colours.black, location: "G2"},
            {type: "pawn", colour: colours.black, location: "H2"},
            {type: "rook", colour: colours.black, location: "A1"},
            {type: "knight", colour: colours.black, location: "B1"},
            {type: "bishop", colour: colours.black, location: "C1"},
            {type: "king", colour: colours.black, location: "D1"},
            {type: "queen", colour: colours.black, location: "E1"},
            {type: "bishop", colour: colours.black, location: "F1"},
            {type: "knight", colour: colours.black, location: "G1"},
            {type: "rook", colour: colours.black, location: "H1"},

            {type: "pawn", colour: colours.white, location: "A7"},
            {type: "pawn", colour: colours.white, location: "B7"},
            {type: "pawn", colour: colours.white, location: "C7"},
            {type: "pawn", colour: colours.white, location: "D7"},
            {type: "pawn", colour: colours.white, location: "E7"},
            {type: "pawn", colour: colours.white, location: "F7"},
            {type: "pawn", colour: colours.white, location: "G7"},
            {type: "pawn", colour: colours.white, location: "H7"},
            {type: "rook", colour: colours.white, location: "A8"},
            {type: "knight", colour: colours.white, location: "B8"},
            {type: "bishop", colour: colours.white, location: "C8"},
            {type: "queen", colour: colours.white, location: "D8"},
            {type: "king", colour: colours.white, location: "E8"},
            {type: "bishop", colour: colours.white, location: "F8"},
            {type: "knight", colour: colours.white, location: "G8"},
            {type: "rook", colour: colours.white, location: "H8"},
        ]
    }

}

Object.freeze(boardConfiguration);

export { colours, boardConfiguration }
