import {boardConfiguration, colours} from './board_configuration.mjs';
import {pieces} from './chess_pieces.mjs';

// Syntatic sugar for adding a method to the property
Function.prototype.method = function(name, func) {
    this.prototype[name] = func;
    return this;
};

Function.method('curry', function() {
    var slice = Array.prototype.slice,
        args = slice.apply(arguments),
        that = this;
    return function() {
        return that.apply(null, args.concat(slice.apply(arguments)));
    };
});

export function Game(id) {
    this.board = [];
    this.id = id;
    this.users = 0;
    this.websockets = [];
    this.validMoves = [];
    this.turnCounter = 0;
    this.turn = colours.white;

    this.initialize = (configuration) => {
        for (let i = 0; i < configuration.length; i++) {
            let array = new Array(configuration.width);
            this.board.push(array);
        }

        this.board.width = configuration.width;

        for (let pieceConfig of configuration.setup) {
            let piece = Object.create(pieces[pieceConfig.type]);
            piece.initialize(pieceConfig.colour, pieceConfig.location);
            piece.board = this.board;
            this.board[piece.verticalPosition][piece.horizontalPosition] = piece;
        }
        return this;
    };
    this.initialize(boardConfiguration.standard);
}




// window.game = new Game(20);
