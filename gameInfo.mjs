// Easy singleton!
export let GameInfo = {
    games: new Map(),
    gamesInitialized: 0,
    gamesStarted: 0,
    gamesOngoing: 0,
    gameId: 0
};
