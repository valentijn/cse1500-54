import {GameInfo} from './gameInfo.mjs';

const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 8080 });
const connectedSockets =  [];
const messageHandler = new Map();

export var WebsocketAPI = {
    sendMessage: (ws, type, content={}) => {
        let response = {messageType: type, userId: null, content};
        ws.send(JSON.stringify(response));
    },

    sendToAll: function (game, type, content={}) {
        game.websockets.forEach((ws) => {
            this.sendMessage(ws, type, content);
        });
    },

    closeGame: function (game) {
        game.websockets.forEach((ws) => {
            ws.close();
        });
    },

    register: function  (name, func) {
        /*
         * All right this is kind of evil but kind of fun, we create a
         * copy of the WebsocketAPI scope together with the type of the
         * function. This way, the registered functions can make use of
         * the Websocket API as if they were part of their own object.
         */
        var registerScope = Object.create(this);
        registerScope.type = name;
        console.debug(`Registering: ${name}`);
        messageHandler.set(name, func.bind(registerScope));
    },

    call: (name, ws, game, userId, content) => {
        messageHandler.get(name)(ws, game, userId, content);
    }
};

wss.on('connection', (ws) => {
    ws.on('message', (message) => {
        let jsonMessage = JSON.parse(message);
        console.log(jsonMessage);
        let game = GameInfo.games.get(parseInt(jsonMessage.gameId));

        // This isn't very nice but it is just to make sure that we don't horribly crash.
        try {
            WebsocketAPI.call(jsonMessage.messageType, ws, game, jsonMessage.userId, jsonMessage.content);
        } catch (e) {
            console.log(e);
        }

    });

    connectedSockets.push(ws);
});
