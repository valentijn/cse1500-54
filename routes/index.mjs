import { Game } from '../public/javascripts/game.mjs';
import { User } from '../public/javascripts/user.mjs';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('splash', {});
});

let id_ = 0;
router.get('/game/createGame', function(req, res) {
    let game = new Game(id_);
    let response = {messageType: 'createGame', content: {}};
    response.content.gameId = id_++;
    response.content.userId = {};
    response.content.pieces = response.game.board;
	
    return res.json(response);
});

router.get('/game/:gameId', function(req, res) {
    console.log(req.params);
});

    module.exports = router;
