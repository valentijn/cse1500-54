import { Game } from '../public/javascripts/game.mjs';
// import { User } from '../public/javascripts/user.mjs';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('splash', {});
});

router.get('/game', function(req, res) {

});

let id_ = 0;
router.get('/createGame', function(req, res) {
    let id = parseInt(req.app.locals.GameInfo.gameId++);
    let game = new Game(id);

    req.app.locals.GameInfo.games.set(id, game);
    req.app.locals.GameInfo.gamesStarted++;


    res.redirect(`/game/${id}`);

    // let response = {messageType: 'createGame', content: {}};
    // response.content.gameId = req.app.locals.GameInfo.gameId++;
    // response.content.userId = {};
    // response.content.pieces = game.board;

    // return res.json(response);
});


router.get('/game/:gameId', function(req, res) {
    console.log(req.params.gameId);
    if (req.app.locals.GameInfo.games.has(parseInt(req.params.gameId)))
        res.render('game', {width: 8, length: 8});
    else
        res.redirect('/createGame');

});

router.get('/game/:gameId/getGame', function (req, res) {
    // console.log(req.app.locals.GameInfo.games.get(0));

    console.log(req);
    let game = req.app.locals.GameInfo.games.get(parseInt(req.params.gameId));
    let response = {messageType: 'joinGame', content: {}};
    response.content.gameId = req.params.gameId;
    response.content.userId = 0;
    response.content.pieces = game.board;

    return res.json(response);
});

module.exports = router;
