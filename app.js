import {colours} from './public/javascripts/board_configuration.mjs';
import {WebsocketAPI} from './websockets.js';
import {GameInfo} from './gameInfo.mjs';

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index.js');

var app = express();
var http = require("http");
var port = process.argv[2];

app.locals.GameInfo = GameInfo;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(__dirname + "/public"));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Set up WebSockets
WebsocketAPI.register("requestMoves", function (ws, game, userId, content) {
    let piece = game.board[content.verticalPosition][content.horizontalPosition];
    let response = {locations: piece.getMoves()};
    game.validMoves = response.locations;
    console.log(response);
    this.sendMessage(ws, this.type, response);
});

WebsocketAPI.register("move", function(ws, game, userId, content) {
    let piece = game.board[content.oldLocation.verticalPosition][content.oldLocation.horizontalPosition];
    let response = {};
    let checkLocation = (location) => {
        return location[0] === content.newLocation.verticalPosition &&
            location[1] === content.newLocation.horizontalPosition;
    };

    // if (content.colour !== game.turn || game.validMoves.find(checkLocation) === undefined)
    // return ws.send(JSON.stringify({messageType: 'rejectMove', userId: userId, content: {}}));

    response.type = piece.type;
    response.colour = piece.colour;
    response.oldLocation = content.oldLocation;
    response.newLocation = content.newLocation;
    var res = piece.move(content.newLocation.verticalPosition, content.newLocation.horizontalPosition);

    if (game.turn === colours.white)
        game.turn = colours.black;
    else
        game.turn = colours.white;

    this.sendToAll(game, "update", response);
    if (res !== undefined && res === "check") {
        this.sendToAll(game, "victory", {userWon: piece.color});
        this.closeGame(game);
    }

    app.locals.GameInfo.games.set(game.gameId, null);
});

WebsocketAPI.register("joinGame", function(ws, game, userId, content) {
    game.websockets.push(ws);
    this.sendMessage(ws, this.type);
});

WebsocketAPI.register("exit", function(ws, game, userId, content) {
    this.sendToAll(game, this.type);
    app.locals.GameInfo.games.set(game.gameId, null);
    this.closeGame(game);

});

http.createServer(app).listen(port);

module.exports = app;
